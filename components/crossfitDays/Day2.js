import React from  'react';
import {Text,View,StyleSheet,Image,SafeAreaView,ScrollView} from 'react-native'

const day2 =()=> {
    return(
        <SafeAreaView>
            <ScrollView>
        <View style={styles.workouts}>
            <View style={{padding:15, flexDirection:'row'}}>
                <Text style={{
                    fontSize:30
                }}>
                8:30   /    9:30
            </Text>
            <Image 
                style={styles.icon}
                 source={require('./../../images/booking-icon.jpg')}
                />
            </View>
            <View>
                <Text
                style={{
                    
                    paddingLeft:10,
                    color:'blue',
                    fontWeight:'bold',
                    fontSize:18
                }}
                >
                    Trainer: Pinco Pallino
                </Text>
                
            </View>
            
            
        </View>
        <View style={styles.workouts}>
            <View style={{padding:15, flexDirection:'row'}}>
                <Text style={{
                    fontSize:30
                }}>
                12:30   /    13:30
            </Text>
            <Image 
                style={styles.icon}
                 source={require('./../../images/booking-icon.jpg')}
                />
            </View>
            <View>
                <Text
                style={{
                    
                    paddingLeft:10,
                    color:'blue',
                    fontWeight:'bold',
                    fontSize:18
                }}
                >
                    Trainer: Pinco Pallino
                </Text>
                
            </View>
            
            
            
        </View>
        <View style={styles.workouts}>
            <View style={{padding:15, flexDirection:'row'}}>
                <Text style={{
                    fontSize:30
                }}>
                14:30   /    15:30
            </Text>
            <Image 
                style={styles.icon}
                 source={require('./../../images/booking-icon.jpg')}
                />
            </View>
            <View>
                <Text
                style={{
                    
                    paddingLeft:10,
                    color:'blue',
                    fontWeight:'bold',
                    fontSize:18
                }}
                >
                    Trainer: Pinco Pallino
                </Text>
                
            </View>
            
            
        </View>
        <View style={styles.workouts}>
            <View style={{padding:15, flexDirection:'row'}}>
                <Text style={{
                    fontSize:30
                }}>
                18:30   /    19:30
            </Text>
            <Image 
                style={styles.icon}
                 source={require('./../../images/booking-icon.jpg')}
                />
            </View>
            <View>
                <Text
                style={{
                    
                    paddingLeft:10,
                    color:'blue',
                    fontWeight:'bold',
                    fontSize:18
                }}
                >
                    Trainer: Pinco Pallino
                </Text>
                
            </View>
            
            
        </View>
        <View style={styles.workouts}>
            <View style={{padding:15, flexDirection:'row'}}>
                <Text style={{
                    fontSize:30
                }}>
                21:30   /    22:30
            </Text>
            <Image 
                style={styles.icon}
                 source={require('./../../images/booking-icon.jpg')}
                />
            </View>
            <View>
                <Text
                style={{
                    
                    paddingLeft:10,
                    color:'blue',
                    fontWeight:'bold',
                    fontSize:18
                }}
                >
                    Trainer: Pinco Pallino
                </Text>
                
            </View>
            
            
        </View>
       
        
        
            </ScrollView>
        </SafeAreaView>    
        )
}

const styles= StyleSheet.create({
    workouts:{ 
        
        marginTop:10,
        backgroundColor:'#aad8d3',
        borderStyle:'solid',
        width: '95%',
        borderWidth:4,
        borderColor:'grey',
        height:115,
        borderColor:'black',
        
        borderRadius:10},
        icon:{
            position:'absolute',
            top:20,
            right:20,
            height:50,
            width:50
        }
   

})
export default day2;