import React,{Component} from 'react';
import {Text,View, StyleSheet,ImageBackground,Image, ScrollView} from 'react-native'

import firebase from 'firebase'

import {data} from './homescreen'



class Profile extends Component {


    constructor(){
        super()
        this.state = {
            name: '/',
            firstName:'/',
            lastName:'/',
            address:'/',
            time1: '/',
            time2: '/',
            time3: '/',
            time4: '/',
            time5: '/',


        }
    }

//     const user = firebase.auth().currentUser.uid;
    
//     const user_name =     data
//     .ref('/users/crossfitLessons1/'+user)
//     .once('value')
//     .then(snapshot => {
//     snapshot.val().name.toString()})
    

//     const seeUserName = () => {
        


//     data
//     .ref('/users/crossfitLessons1/'+user)
//     .once('value')
//     .then(snapshot => {
//     console.log(snapshot.val().name)
    
//   }).catch((error) => {
//   console.error(error);
// });
//   }


//       const seeUserLesson = () => {
        


//     data
//     .ref('/users/crossfitLessons1/'+user)
//     .once('value')
//     .then(snapshot => {
//     console.log(snapshot.val().time)
    
//   }).catch((error) => {
//   console.error(error);
// });
//   }






 
// const dbRef = firebase.database().ref();
// dbRef.child("users/crossfitLesson"+user).child(user.time).get().then((snapshot) => {
//   if (snapshot.exists()) {
//     console.log(snapshot.val());
//   } else {
//     console.log("No data available");
//   }
// })


  //  }


  componentDidMount(){

     const user = firebase.auth().currentUser.uid;

      const rootRef = firebase.database().ref().child('users')
      const nameRef = rootRef.child('crossfitLessons1/'+user+'/name');
      nameRef.on('value', snap => {
          this.setState({
              name: snap.val()
          })
      })

      const timeRef = rootRef.child('crossfitLessons1/'+user+'/time');
      timeRef.on('value', snap => {
          this.setState({
              time1: snap.val()
          })
      })

            const timeRef2 = rootRef.child('crossfitLessons2/'+user+'/time');
      timeRef2.on('value', snap => {
          this.setState({
              time2: snap.val()
          })
      })
            const timeRef3 = rootRef.child('crossfitLessons3/'+user+'/time');
      timeRef3.on('value', snap => {
          this.setState({
              time3: snap.val()
          })
      })
            const timeRef4 = rootRef.child('crossfitLessons4/'+user+'/time');
      timeRef4.on('value', snap => {
          this.setState({
              time4: snap.val()
          })
      })
            const timeRef5 = rootRef.child('crossfitLessons5/'+user+'/time');
      timeRef5.on('value', snap => {
          this.setState({
              time5: snap.val()
          })
      })

      const firstNameRef = rootRef.child(user +'/name');
      firstNameRef.on('value', snap => {
          this.setState({
              firstName: snap.val()
          })
      })

      const LastNameRef = rootRef.child(user +'/surname');
      LastNameRef.on('value', snap => {
          this.setState({
              lastName: snap.val()
          })
      })

       const addressRef = rootRef.child(user +'/address');
      addressRef.on('value', snap => {
          this.setState({
              address: snap.val()
          })
      })


  }

render(){
    return(
        <>
        <ScrollView
        style={styles.container}>
        

            <Text
            style={{fontSize:40, justifyContent:'center'}}
            // onPress={seeUserName }
            >
                
                Your Profile 
            </Text>
            <View>
                <View>
                    <ImageBackground 
                        style ={styles.training_image}
                        source={require('./../images/home_image.jpg')}>
                            
                        </ImageBackground>
                        <Image 
                            style={styles.avatar}
                            source={require('./../images/profile1.jpg')}
                        />
                        
                       
                </View>
                 

            </View>
            
           
            <View
            style={{        backgroundColor:'#aad8d3',
            height:700
}}
            >
                <View
                style={{justifyContent:'center',
                alignContent:'center'
            }}
                >

            <View
                style={styles.profileContent1}
            >
                <Text
                style={{justifyContent:'center',
                alignContent:'center'}}
            >
                First Name: {this.state.firstName}
            </Text>
                

            </View>

            <View
                style={styles.profileContent}
            >
                <Text
                style={{justifyContent:'center',
                alignContent:'center'}}
            >
                Last Name: {this.state.lastName}
            </Text>
                

            </View>

            <View
                style={styles.profileContent}
            >
                <Text
                style={{justifyContent:'center',
                alignContent:'center'}}
            >
                Address: {this.state.address}
            </Text>
                

            </View>
            <View
                style={styles.profileContent}
            >
                <Text
                style={{justifyContent:'center',
                alignContent:'center'}}
            >
                Email: {this.state.name}
            </Text>
                

            </View>

            <View
                style={styles.profileContent}
            >
                <Text
                style={{justifyContent:'center',
                alignContent:'center'}}
            >
                Lessons this week: 
            </Text>
            <Text>
                {this.state.time1} 
            </Text>
            <Text>
                {this.state.time2} 
            </Text>
            <Text>
                {this.state.time3} 
            </Text>
            <Text>
                {this.state.time4} 
            </Text>
            <Text>
                {this.state.time5} 
            </Text>
                

            </View>


                </View>


             </View>

            
           </ScrollView>
            

            </>
        
    )
    
    }}


const styles= StyleSheet.create({
    container:{
        backgroundColor:"#D3D3D3",
        height:80,

        
    },training_image:{
        justifyContent:'center',
        alignItems:'center',
        height:190,
        width: 420,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5
    },
    avatar:{
        width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
    position: 'absolute',
    marginTop:130,
    zIndex:5
    },
    profileContent1:{
        borderRadius:5,
        borderColor:'black',
        borderWidth:5,
        backgroundColor:'white',
        marginBottom:20,
        marginTop:80,
         marginLeft:20,
        marginRight:20,
        padding:10
    },
    profileContent:{
        borderRadius:5,
        borderColor:'black',
        borderWidth:5,
        backgroundColor:'white',
        marginBottom:20,
        marginLeft:20,
        marginRight:20,
        padding:10
    }

})
export default Profile;


